package siam_scraper

import (
	"encoding/json"
	"github.com/gocolly/colly"
	"log"
	"strconv"
	"errors"
)

const url = "https://siam.ub.ac.id"

func LoginSiam(scraper *colly.Collector, data map[string]string) error {
	err:=scraper.Post(url,data)
	if err!=nil{
		return err
	}
	return nil
}

func Init() *colly.Collector {
	c := colly.NewCollector()
	c.UserAgent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"
	return c
}

func GetRekap(scraper *colly.Collector) ([]byte,error){
	nilaiScale:=make(map[string]float32)
	nilaiScale["A"]=4.00
	nilaiScale["B+"]=3.50
	nilaiScale["B"]=3.00
	nilaiScale["C+"]=2.50
	nilaiScale["C"]=2.00
	nilaiScale["D+"]=1.50
	nilaiScale["D"]=1.00
	nilaiScale["E"]=0.00
	nilaiScale["K"]=0.00
	datamahasiswa := DataMahasiswa{}
	var counter int = 2
	rhs := RekapHasilStudi{}
	scraper.OnXML("/html/body/table[2]/tbody/tr[1]/td[2]/div[2]", func(e *colly.XMLElement) {
		for {
			if e.ChildText("/table/tbody/tr/td/table/tbody/tr["+strconv.Itoa(counter)+"]")==""{
				break
			}
			rhs.Kode=e.ChildText("/table/tbody/tr/td/table/tbody/tr["+strconv.Itoa(counter)+"]/td[1]")
			rhs.Matkul=e.ChildText("/table/tbody/tr/td/table/tbody/tr["+strconv.Itoa(counter)+"]/td[2]")
			rhs.JumlahSKS=e.ChildText("/table/tbody/tr/td/table/tbody/tr["+strconv.Itoa(counter)+"]/td[3]")
			rhs.Nilai=e.ChildText("/table/tbody/tr/td/table/tbody/tr["+strconv.Itoa(counter)+"]/td[5]")
			datamahasiswa.AddNilai(rhs)
			datamahasiswa.TotalSKS(strconv.Atoi(rhs.JumlahSKS))
			datamahasiswa.HitungIPK(nilaiScale[rhs.Nilai],rhs.JumlahSKS)
			counter++
		}
	})
	scraper.OnHTML(`div[class=text-green]`, func(e *colly.HTMLElement) {
	})
	scraper.OnXML("/html/body/table[2]/tbody/tr[1]/td[2]/table[1]/tbody/tr[1]/td[3]/div/div[1]", func(e *colly.XMLElement) {
		datamahasiswa.Nim=e.Text
	})
	scraper.OnXML("/html/body/table[2]/tbody/tr[1]/td[2]/table[1]/tbody/tr[1]/td[3]/div/div[2]", func(e *colly.XMLElement) {
		datamahasiswa.Nama=e.Text
	})
	scraper.Visit(url+"/rekapstudi.php")
	if datamahasiswa.Nama==""{
		return nil,errors.New("Data not found")
	}
	datamahasiswa.IPK=datamahasiswa.IPK/float32(datamahasiswa.JumlahSKS)
	datajson,err:=json.Marshal(datamahasiswa)
	checkErr(err)

	return datajson,nil
}

func checkErr(err error) {
	if err!=nil {
		log.Fatal(err)
	}
}